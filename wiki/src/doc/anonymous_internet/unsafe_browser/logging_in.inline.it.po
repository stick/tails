# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-06-25 02:00+0000\n"
"PO-Revision-Date: 2021-03-03 08:44+0100\n"
"Last-Translator: la_r_go* <largo@tracciabi.li>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
msgid "To log in to a captive portal:"
msgstr "Accedere a un portale controllato:"

#. type: Bullet: '1. '
msgid "Try visiting any website using the *Unsafe Browser*."
msgstr "Prova a navigare tra i siti web usando un *Browser non Sicuro*."

#. type: Plain text
#, no-wrap
msgid ""
"   Choose a website that is common in your location, for example a search\n"
"   engine or news site.\n"
msgstr ""
"   Scegli un sito web abituale in base alla tua ubicazione, ad esempio un motore\n"
"   di ricerca o un sito di notizie.\n"

#. type: Bullet: '1. '
msgid "You should be redirected to the captive portal instead of the website."
msgstr ""
"Dovresti essere reindirizzato al portale controllato invece che al sito web."

#. type: Bullet: '1. '
msgid "After you logged in to the captive portal, Tor should start."
msgstr ""
"Dopo aver effettuato l'accesso al portale controllato, Tor dovrebbe avviarsi."

#. type: Bullet: '1. '
msgid "After Tor is ready, close the *Unsafe Browser*."
msgstr "Quando Tor è pronto, chiudi il *Browser non Sicuro*."

#. type: Plain text
#, no-wrap
msgid "   You can use *Tor Browser* and any other application as usual.\n"
msgstr "   Puoi usare *Tor Browser* e qualsiasi altra applicazione come al solito.\n"
