# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-09-25 10:43+0000\n"
"PO-Revision-Date: 2015-10-10 13:50+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/"
"first_steps_persistence_checkfs/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Check the file system of the persistent volume\"]]\n"
msgid "[[!meta title=\"Checking the file system of the Persistent Storage\"]]\n"
msgstr "[[!meta title=\"بررسی فایل سیستمی درایو مانا\"]]\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "In rare occasions, you might have to perform a file system check to "
#| "repair a broken persistent volume."
msgid ""
"In rare occasions, you might have to perform a file system check to repair a "
"broken Persistent Storage."
msgstr ""
"در مواردی نادر ممکن است مجبور باشید برای تعمیر یک درایو مانای خراب فایل "
"سیستمی را بررسی کنید."

#. type: Title =
#, no-wrap
msgid "Unlock the Persistent Storage"
msgstr ""

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "Start Tails, with persistence disabled, and [[set up an administration "
#| "password|welcome_screen/administration_password]]."
msgid ""
"When starting Tails, keep the Persistent Storage locked and [[set up an "
"administration password|doc/first_steps/welcome_screen/"
"administration_password]]."
msgstr ""
"تیلز را در حالت مانای غیرفعال راه‌اندازی کرده و [[یک گذرواژهٔ مدیریتی ایجاد "
"کنید|welcome_screen/administration_password]]."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "1. Choose\n"
#| "   <span class=\"menuchoice\">\n"
#| "     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#| "     <span class=\"guisubmenu\">Accessories</span>&nbsp;▸\n"
#| "     <span class=\"guimenuitem\">Disk Utility</span>\n"
#| "   </span>\n"
#| "   to open the <span class=\"application\">GNOME Disk Utility</span>.\n"
msgid ""
"1. Choose\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"     <span class=\"guisubmenu\">Utilities</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Disks</span>\n"
"   </span>\n"
"   to open <span class=\"application\">GNOME Disks</span>.\n"
msgstr ""
"۱. این گزینه را انتخاب کنید\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">ابزارها</span>&nbsp;◀\n"
"     <span class=\"guisubmenu\">موارد کمکی</span>&nbsp;◀\n"
"     <span class=\"guimenuitem\">ابزارهای دیسک</span>\n"
"   </span>\n"
"   تا <span class=\"application\">ابزار دیسک گنوم</span> باز شود.\n"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "In the left pane, click on the device corresponding to your Tails device."
msgid ""
"In the left pane, click on the device corresponding to your Tails USB stick."
msgstr "در سمت راست روی دستگاه مرتبط با دستگاه تیلز خود کلیک کنید."

#. type: Bullet: '1. '
#, fuzzy
msgid ""
"In the right pane, click on the partition labeled as <span class=\"guilabel"
"\">TailsData LUKS</span>."
msgstr ""
"در سمت چپ روی بخش موسوم به <span class=\"guilabel\">Encrypted</span> کلیک "
"کنید. <span class=\"guilabel\">نام پارتیشن</span> باید <span class=\"label"
"\">TailsData</span> باشد."

#. type: Bullet: '1. '
msgid ""
"Click on **Unlock** to unlock the Persistent Storage. Enter the passphrase "
"of the Persistent Storage and click **Unlock** again."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"In the confirmation dialog, enter your administration password and click "
"<span class=\"guilabel\">Authenticate</span>."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Click on the <span class=\"guilabel\">TailsData Ext4</span> partition that "
"appears below the <span class=\"guilabel\">TailsData LUKS</span> partition."
msgstr ""
"روی پارتیشن <span class=\"guilabel\">TailsData</span> که پایین <span class="
"\"guilabel\">درایو رمزگذاری‌شده</span> دیده می‌شود کلیک کنید."

#. type: Bullet: '1. '
msgid ""
"Identify the device name of your Persistent Storage. The device name appears "
"below the list of volumes.  It should look like <code>/dev/mapper/luks-"
"xxxxxxxx</code>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   Triple-click to select it and press **Ctrl+C** to\n"
"   copy it to the clipboard.\n"
msgstr ""

#. type: Title =
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Check the file system of the persistent volume\"]]\n"
msgid "Check the file system using the terminal"
msgstr "[[!meta title=\"بررسی فایل سیستمی درایو مانا\"]]\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "1. Choose\n"
#| "   <span class=\"menuchoice\">\n"
#| "     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
#| "     <span class=\"guisubmenu\">Accessories</span>&nbsp;▸\n"
#| "     <span class=\"guimenuitem\">Disk Utility</span>\n"
#| "   </span>\n"
#| "   to open the <span class=\"application\">GNOME Disk Utility</span>.\n"
msgid ""
"1. Choose\n"
"    <span class=\"menuchoice\">\n"
"      <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"      <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"      <span class=\"guimenuitem\">Root Terminal</span></span>\n"
"   and enter your administration password to open a root terminal.\n"
msgstr ""
"۱. این گزینه را انتخاب کنید\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">ابزارها</span>&nbsp;◀\n"
"     <span class=\"guisubmenu\">موارد کمکی</span>&nbsp;◀\n"
"     <span class=\"guimenuitem\">ابزارهای دیسک</span>\n"
"   </span>\n"
"   تا <span class=\"application\">ابزار دیسک گنوم</span> باز شود.\n"

#. type: Bullet: '1. '
msgid ""
"In the terminal, execute the following command, replacing `[device]` with "
"the device name found in step 8:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "        fsck -y [device]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   To do so, you can type <span class=\"command\">fsck -y&nbsp;</span> and press\n"
"   <span class=\"keycap\">Shift+Ctrl+V</span> to paste the device name from the\n"
"   clipboard.\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"If the file system is free of errors, the last line from the output of <span "
"class=\"command\">fsck</span> starts with <span class=\"command\">TailsData: "
"clean</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   If the file system has errors, <span class=\"command\">fsck</span> tries to\n"
"   fix them automatically. After it finishes, you can try executing the same\n"
"   command again to check if all errors are solved.\n"
msgstr ""

#, fuzzy
#~ msgid ""
#~ "Click on the <span class=\"guimenu\">[[!img lib/unlock.png alt=\"Unlock\" "
#~ "class=\"symbolic\" link=\"no\"]]</span> button to unlock the persistent "
#~ "volume. Enter the passphrase of the persistent volume and click <span "
#~ "class=\"guilabel\">Unlock</span>."
#~ msgstr ""
#~ "روی <span class=\"guilabel\">آزاد کردن درایو</span> کلیک کنید تا درایو "
#~ "مانا آزاد شود. گذرواژهٔ درایو مانای قدیمی را وارد کرده و روی <span class="
#~ "\"guilabel\">آزاد کردن</span> کلیک کنید."

#~ msgid "Click on <span class=\"guilabel\">Check Filesystem</span>."
#~ msgstr "روی <span class=\"guilabel\">بررسی فایل سیستمی</span> کلیک کنید."
