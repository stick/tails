[[!meta title="Calendar"]]

All times are referenced in [[!wikipedia UTC]].

# 2021 Q2

* 2021-04-13, 20:00: Technical Writing session on warnings ([[!tails_ticket 8845]])

* 2021-04-20: **Release 4.18** (Firefox 78.10) — intrigeri is the RM, nodens is the TR

* 2021-04-21, 14:00: Accounting and Management team meeting

- 2021-04-22: 4.19~beta1 ([[!tails_ticket 18260]], [S08 milestone 1](https://gitlab.tails.boum.org/groups/tails/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=D%3ASponsor08&milestone_title=Tails_4.19)) — anonym is the RM

* 2021-04-22, 16:00: Healing working group meeting

- 2021-05-06: 4.19~rc1 ([S08 milestone 1](https://gitlab.tails.boum.org/groups/tails/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=D%3ASponsor08&milestone_title=Tails_4.19)) — anonym is the RM

* 2021-05-18: **Release 4.19** (Firefox 78.11) — anonym is the RM

* 2021-06-15: **Release 4.20** (Firefox 78.12) — intrigeri is the RM

# 2021 Q3

* 2021-07-13: **Release 4.21** (Firefox 78.13)

- by July 27: **Release 4.22~rc1**, that includes
  [S08 milestone 3](https://gitlab.tails.boum.org/groups/tails/-/boards?scope=all&utf8=%E2%9C%93&label_name[]=D%3ASponsor08&milestone_title=Tails_4.22)

* 2021-08-10: **Release 4.22** (Firefox 78.14)

* 2021-09-07: **Release 4.23** (Firefox 78.15)

# 2021 Q4

* 2021-10-05: **Release 4.24** (Firefox 91.3)

* 2021-11-02: **Release 4.25** (Firefox 91.4)

* 2021-12-07: **Release 4.26** (Firefox 91.5)

# 2022 Q1

* 2022-01-11: **Release 4.27** (Firefox 91.6)
