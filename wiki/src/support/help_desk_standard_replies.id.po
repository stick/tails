# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-02-12 15:32+0000\n"
"PO-Revision-Date: 2020-12-24 18:45+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 3.11.3\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Help Desk's standard replies\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Standard reply"
msgstr ""

#. type: Plain text
msgid "Dear Tails user,"
msgstr ""

#. type: Plain text
msgid ""
"We are a very small team working on very limited human resources, with "
"regards to that, we may not be able to reply to every request we receive, "
"or, we may not reply in a timely manner. In advance, we apologize for that."
msgstr ""

#. type: Plain text
msgid "You should find many resources on our support page:"
msgstr ""

#. type: Plain text
msgid "https://tails.boum.org/support"
msgstr ""

#. type: Plain text
msgid ""
"From there you should find links to your complete documentation, our FAQ and "
"our known issues page, that includes a page dedicated to graphic card issues."
msgstr ""

#. type: Plain text
msgid ""
"If you are getting the \"Error starting GDM\", please read the provided "
"link: https://tails.boum.org/gdm"
msgstr ""

#. type: Plain text
msgid ""
"It is very difficult to investigate such problems without having access to "
"the affected computer. Furthermore, even if we managed to investigate the "
"problem, unfortunately we would lack resources to solve it."
msgstr ""

#. type: Plain text
msgid "We need more information from you in 2 cases:"
msgstr ""

#. type: Plain text
msgid "- **If this problem did not happen with an older version of Tails**:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  Please tell us which older version worked better.\n"
msgstr ""

#. type: Plain text
msgid "- **If you find a way to workaround the problem**:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "  Please tell us, and we will document it so that other affected users benefit from your findings.\n"
msgstr ""

#. type: Plain text
msgid ""
"Our best hope is that a future Linux driver update will solve the problem."
msgstr ""

#. type: Plain text
msgid ""
"For advanced Linux users: if you want to dig deeper on your own, you can "
"take it upstream after reproducing on a recent, non-Tails Linux system."
msgstr ""

#. type: Plain text
msgid ""
"If your issue is related to the Tor Network itself, please consider "
"contacting the The Tor Project instead."
msgstr ""

#. type: Plain text
msgid ""
"If your issue is related to software included in Tails, please consider "
"contacting their support channels."
msgstr ""

#. type: Plain text
msgid "Thanks for your understanding, and have a very nice day."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Reply to hardware support reports for which we can't do anything"
msgstr ""

#. type: Plain text
msgid "Hi,"
msgstr ""

#. type: Plain text
msgid ""
"Thank you for sharing your experience with us. We are sorry that Tails does "
"not work well on this computer."
msgstr ""

#. type: Plain text
msgid ""
"If you did not do it yet, please read our Support page: https://tails.boum."
"org/support/"
msgstr ""

#. type: Plain text
msgid "Thanks for your understanding :)"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Known issue where we need feedback"
msgstr ""

#. type: Plain text
msgid "Thanks for your report,"
msgstr ""

#. type: Plain text
msgid ""
"I think that your are experiencing this issue that has already been reported:"
msgstr ""

#. type: Plain text
msgid "INSERT LINK TO ISSUE"
msgstr ""

#. type: Plain text
msgid ""
"Can you confirm that your issue is the same and provide requested feedback "
"to GitLab issue?"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Not enough information"
msgstr ""

#. type: Plain text
msgid ""
"Thanks for taking the time to report this problem. Unfortunately we can't "
"help you for the moment because your description doesn't include enough "
"information."
msgstr ""

#. type: Plain text
msgid "Please read our bug reporting instructions:"
msgstr ""

#. type: Plain text
msgid "https://tails.boum.org/doc/first_steps/bug_reporting"
msgstr ""

#. type: Plain text
msgid ""
"Your problem may be already known, please consult the known issues page:"
msgstr ""

#. type: Plain text
msgid "https://tails.boum.org/support/known_issues/"
msgstr ""

#. type: Plain text
msgid ""
"We'd be grateful if you would then provide a more complete description of "
"the problem:"
msgstr ""

#. type: Plain text
msgid "INSERT SUPPORT QUESTION HERE"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Apple hardware support (Mac)"
msgstr ""

#. type: Plain text
msgid ""
"See ticket here for up to date details: <https://gitlab.tails.boum.org/tails/"
"tails/-/issues/17640>."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Reply for users who repeatedly treat us as search engines"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"> This reply is not advisable for first time reports, only for\n"
"> users that keep asking questions already covered by our\n"
"> documentation.\n"
msgstr ""

#. type: Plain text
msgid ""
"We are a very small team working on very limited human resources. A lot of "
"other people are asking for our help on our various support channels.  User "
"support is an important task and is quite high in our priorities.  But all "
"the time that we spend doing user support is time that we cannot dedicate to "
"improving Tails as such. So please, take this into consideration when "
"writing to us..."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Not a Tails-dev question"
msgstr ""

#. type: Plain text
msgid ""
"This is the mailing-list for Tails development.  For usage and support "
"questions, please see instead:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "\thttps://tails.boum.org/support/\n"
msgstr ""

#. type: Plain text
msgid "Thanks!"
msgstr ""

#. type: Title =
#, no-wrap
msgid "No OpenPGP key on the keyservers"
msgstr ""

#. type: Plain text
msgid ""
"We could not find an OpenPGP key corresponding to this email address on the "
"keyservers.  Please send us a valid public OpenPGP key, or publish it on the "
"keyservers."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Howto collaborate with Tails"
msgstr ""

#. type: Plain text
msgid "Please see https://tails.boum.org/contribute/"
msgstr ""

#. type: Plain text
msgid "Cheers,"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Tails mirror is broken or scary SSL message"
msgstr ""

#. type: Plain text
msgid ""
"Thanks for reporting this. We have transferred these details to the team "
"that works on this at tails-mirrors@boum.org."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Templates for requesting feedback on GitLab issues"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "Asking a user to try to reproduce the issue with Debian-live"
msgstr ""

#. type: Plain text
msgid ""
"Could you try to start a live version of Debian on your computer and see if "
"you can reproduce the issue?"
msgstr ""

#. type: Plain text
msgid "https://tails.boum.org/doc/first_steps/bug_reporting/#debian"
msgstr ""

#. type: Plain text
msgid ""
"With your help this issue could hopefully then be fixed in a future version "
"of Linux and thus Tails."
msgstr ""

#. type: Plain text
msgid "Thanks in advance!"
msgstr ""
